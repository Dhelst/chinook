package com.example.chinook.Models;

//I have written the record to take the column from database sql
public record Customer(
                       String first_name,
                       String last_name,
                       String country,
                       String postal_code,
                       String phone,
                       String email
){
}
