package com.example.chinook.Repository.Interfaces;

import com.example.chinook.Models.Customer;
import com.example.chinook.Models.CustomerCountry;
import com.example.chinook.Models.CustomerGenre;
import com.example.chinook.Models.CustomerSpender;

import java.sql.SQLException;
import java.util.List;

public interface CustomerRepository extends RepositoryCRUD<Customer, Integer> {


    int update(Customer object);

    List<Customer> findByName(String name);



    CustomerCountry countryCustomer() throws SQLException;


    List<CustomerCountry> countryCustomer(int country) throws SQLException;

    CustomerSpender highestSpender() throws SQLException;


    CustomerGenre customerGenre() throws SQLException;


    List<CustomerCountry> countryCustomer(String couyntry, int counter) throws SQLException;

    List <Customer> pageCustomers(int limit, int offset);


    int addCustomer(Customer customer) throws SQLException;


    int updateExistingCustomer(Customer customer) throws SQLException;

    int updateExistingCustomer(Customer customer, int id) throws SQLException;


    List<Customer> pageCustomers();

    List<Customer> addNewCustomers();

    List<Customer> addNewCustomers(int id, String first_name, String last_name, String country, String postal_code, String phone, String email);

    List<Customer> addNewCustomers(String first_name, String last_name, String country, String postal_code, String phone, String email);

    List<Customer> addNewCustomers(Customer customer);

    int updateExistingCustomer(int id) throws SQLException;

    long getId();

    List<Customer> getCustomers();

}
