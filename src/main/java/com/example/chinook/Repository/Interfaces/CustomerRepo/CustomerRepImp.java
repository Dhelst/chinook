package com.example.chinook.Repository.Interfaces.CustomerRepo;

import com.example.chinook.Models.Customer;
import com.example.chinook.Models.CustomerCountry;
import com.example.chinook.Models.CustomerGenre;
import com.example.chinook.Models.CustomerSpender;
import com.example.chinook.Repository.Interfaces.CustomerRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


// Repository a mechanism for encapsulating storage, retrieval, and search behavior which emulates a collection of objects
@Repository
public class CustomerRepImp implements CustomerRepository {
    private final String url;
    private final String username;
    private final String password;

    public CustomerRepImp(
            @Value("${spring.datasource.url}") String url,
            @Value("${spring.datasource.username}") String username,
            @Value("${spring.datasource.password}") String password
    ) {
        this.url = url;
        this.username = username;
        this.password = password;
    }
    // test the  method if it is running and if we are ready to start to code
    public void test() {
        System.out.println("Attempt to connect to  database");
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            System.out.println("Connection success");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    // mwthod to get all the customers from database
    // override from the CRUD CREATE READ DELETE
    @Override
    public List<Customer> findAll() {
        String sql = "SELECT * FROM customer";
        return getCustomers(sql);
    }

    //JDBC to get the data from sql and to get the rows
    private List<Customer> getCustomers(String sql) {
        List<Customer> customers = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Customer customer = new Customer(
                        //resultSet.getInt("customer_id"),
                        resultSet.getString("first_name"),
                        resultSet.getString("last_name"),
                        resultSet.getString("country"),
                        resultSet.getString("postal_code"),
                        resultSet.getString("phone"),
                        resultSet.getString("email")
                );
                customers.add(customer);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customers;
    }
    // method to get a customer by id
    // override from the CRUD CREATE READ DELETE
    @Override
    public Customer findById(Integer id) {
        String sql = "SELECT * FROM customer WHERE customer_id = " + id;
        Customer customer = null;
        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                customer = new Customer(
                        //resultSet.getInt("customer_id"),
                        resultSet.getString("first_name"),
                        resultSet.getString("last_name"),
                        resultSet.getString("country"),
                        resultSet.getString("postal_code"),
                        resultSet.getString("phone"),
                        resultSet.getString("email")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customer;
    }

    @Override
    public int update(Customer object) {
        return 0;
    }

    // method to get a customer by name
    // override from the CRUD CREATE READ DELETE
    //used again method getcustomers
    @Override
    public List<Customer> findByName(String name) {
        String sql = "SELECT * FROM customer WHERE LOWER(first_name) LIKE LOWER('" + name + "')";
        return getCustomers(sql);
    }

    @Override
    public List<CustomerCountry> countryCustomer(String couyntry, int counter) throws SQLException {
        return null;
    }

    // method to get a page customer by using limit  and offset
    // override from the CRUD CREATE READ DELETE
    //used again method getcustomers
    @Override
    public List<Customer> pageCustomers(int limit, int offset) {
        String sql = "SELECT * FROM customer LIMIT " + limit + "  OFFSET  " + offset;
        return getCustomers(sql);
    }

    @Override
    public int insert(Customer object) {
        return 0;
    }

    @Override
    public int Update(Customer object) {
        return 0;
    }

    // method to add a customer / update
    // override from the CRUD CREATE READ DELETE
    //used  method addnewcustomer
    @Override
    public int addCustomer(Customer customer) throws SQLException {
        String sql = "INSERT INTO customer( first_name, last_name, country, postal_code, phone, email) VALUES (?,?,?,?,?,?)";
        return AddNewCustomer(customer, sql);
    }

    @Override
    public int updateExistingCustomer(Customer customer) throws SQLException {
        return 0;
    }

    @Override
    public int updateExistingCustomer(Customer customer, int id) throws SQLException {
        return 0;
    }


    private int AddNewCustomer(Customer customer, String sql) throws SQLException {
        int result = 0;
        Connection conn = DriverManager.getConnection(url, username, password);
        PreparedStatement statement = conn.prepareStatement(sql);
        PreparedStatement preparedStatement = conn.prepareStatement(sql);
        preparedStatement.setString(1, customer.first_name());
        preparedStatement.setString(2, customer.last_name());
        preparedStatement.setString(3, customer.country());
        preparedStatement.setString(4, customer.postal_code());
        preparedStatement.setString(5, customer.phone());
        preparedStatement.setString(6, customer.email());

        result = preparedStatement.executeUpdate();
        return result;

    }


    // method to get most country
    // override from the CRUD CREATE READ DELETE
    //used again method getCountry from the same record (Customer)
    @Override
    public CustomerCountry countryCustomer() throws SQLException {
        String sql = "SELECT country, COUNT(*) AS counter FROM customer GROUP BY country ORDER BY counter DESC LIMIT 1 ";
        return getCountry(sql);

    }

    @Override
    public List<CustomerCountry> countryCustomer(int country) throws SQLException {
        return null;
    }

    private CustomerCountry getCountry(String sql) throws SQLException {
        CustomerCountry customerCountry = null;
        Connection conn = DriverManager.getConnection(url, username, password);
        PreparedStatement preparedStatement = conn.prepareStatement(sql);
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            customerCountry = new CustomerCountry(
                    resultSet.getString("country"),
                    resultSet.getInt("counter")
            );
        }
        return customerCountry;


    }




    // method to get total invoices
    // override from the CRUD CREATE READ DELETE
    //used again method getcustomer Spender from the same record (Customer)
    @Override
    public CustomerSpender highestSpender() throws SQLException {
        String sql = "SELECT  customer.first_name,last_name, SUM(total) FROM invoice INNER JOIN customer ON customer.customer_id = invoice.customer_id GROUP BY customer.customer_id ORDER BY SUM(total) DESC LIMIT 1";
        return getCustomerSpender(sql);
    }

    private CustomerSpender getCustomerSpender(String sql) throws SQLException {
        CustomerSpender customerSpender = null;
        Connection conn = DriverManager.getConnection(url, username, password);
        PreparedStatement preparedStatement = conn.prepareStatement(sql);
        ResultSet resultSet = preparedStatement.executeQuery();
        if (resultSet.next()) {
            customerSpender = new CustomerSpender(
                    resultSet.getDouble("sum"),
                    resultSet.getString("first_name"),
                    resultSet.getString("last_name")
            );
        }
        return customerSpender;

    }


    // method to get most genre tracks
    // override from the CRUD CREATE READ DELETE
    //used again method custom genre Spender from the same record (Customer)
    @Override
    public CustomerGenre customerGenre() throws SQLException {
        String sql = "SELECT c.customer_id, c.first_name, c.last_name, g.name, COUNT(*) as genre_count \n" +
                "FROM customer c\n" +
                "         JOIN invoice i ON c.customer_id = i.customer_id\n" +
                "         JOIN invoice_line il on i.invoice_id = il.invoice_id\n" +
                "         JOIN track t on il.track_id = t.track_id\n" +
                "         JOIN genre g on t.genre_id = g.genre_id\n" +
                "GROUP BY c.customer_id, g.name, c.first_name, c.last_name, g.name\n" +
                "ORDER BY genre_count DESC\n" +
                "\n" +
                "LIMIT 1";
        return getPopularGenre(sql);}
    private CustomerGenre getPopularGenre(String sql) throws SQLException {
        CustomerGenre customerGenre = null;
        Connection conn = DriverManager.getConnection(url, username, password);
        PreparedStatement preparedStatement = conn.prepareStatement(sql);
        ResultSet resultSet = preparedStatement.executeQuery();
        if (resultSet.next()) {
            customerGenre = new CustomerGenre(
                    resultSet.getInt("customer_id"),
                    resultSet.getString("first_name"),
                    resultSet.getString("last_name"),
                    resultSet.getString("name"),
                    resultSet.getString("genre_count")


            );
        }
        return customerGenre;
    }




    @Override
    public List<Customer> pageCustomers() {
        return getCustomers();
    }



    @Override
    public List<Customer> addNewCustomers() {
        return null;
    }

    @Override
    public List<Customer> addNewCustomers(int id, String first_name, String last_name, String country, String postal_code, String phone, String email) {
        return null;
    }


    @Override
    public List<Customer> addNewCustomers(String first_name, String last_name, String country, String postal_code, String phone, String email) {
        return null;
    }

    @Override
    public List<Customer> addNewCustomers(Customer customer) {
        return null;
    }
    @Override
    public int updateExistingCustomer(int id) throws SQLException {
        return 0;
    }

    @Override
    public long getId() {
        return 0;
    }

    @Override
    public List<Customer> getCustomers() {
        return null;
    }











}


