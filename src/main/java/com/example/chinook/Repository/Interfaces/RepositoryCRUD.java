package com.example.chinook.Repository.Interfaces;

import com.example.chinook.Models.Customer;

import java.util.List;

public interface RepositoryCRUD<T, U> {
    List<T> findAll();
    T findById(U id);

    List<T> pageCustomers(int limit, int offset);

    int insert (T object);

    int Update (T object);


}
