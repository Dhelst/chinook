package com.example.chinook.Runner;

import com.example.chinook.Models.Customer;
import com.example.chinook.Repository.Interfaces.CustomerRepository;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;


@Component
public class AppRunner implements ApplicationRunner {

    private final CustomerRepository customerrep;
    public AppRunner(CustomerRepository customerrep) {
        this.customerrep = customerrep;
    }



    @Override
    public void run(ApplicationArguments args) throws Exception {
        System.out.println("Spring run");
        System.out.println(customerrep.findAll());
        System.out.println();
        System.out.println(customerrep.findById(20));
        System.out.println();
        System.out.println(customerrep.findByName("Leonie"));
        System.out.println();
        System.out.println(customerrep.pageCustomers(5,2));
        System.out.println();
        customerrep.addCustomer(new Customer("daniel","helstad", "norway", "0652", "45917277", "daniel.helstad1@gmail.com"));
        System.out.println();
        System.out.println(customerrep.updateExistingCustomer(new Customer("danielsun","helfcfcstad", "norwfccay", "064552", "4591745277", "daniel.hecflstad1@gmail.com" ),1));
        System.out.println();
        System.out.println(customerrep.countryCustomer());
        System.out.println();
        System.out.println(customerrep.highestSpender());
        System.out.println();
        System.out.println(customerrep.customerGenre());
    }
}
