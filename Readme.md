
# Superhero sql & Chinook


## SuperheroeDb
README


This project is to create a PgAdmin database called "SuperheroesDb" and several scripts that can be run to create tables, add relationships to the tables, and then populate the tables with data. The theme of the database is surrounding superheroes.
Requirements

   - PgAdmin
   - SQL

### Installation

    Create a new database in PgAdmin called "SuperheroesDb".
    Run the scripts provided in the following order:
        01. tableCreate.sql: This script contains statements to create the tables Superhero, Assistant, and Power, and set up their primary keys.
        02. relationshipSuperheroAssistant.sql: This script contains statements to ALTER the Assistant table to add a foreign key representing the superhero this assistant is linked to, and setup the constraint.
        03. relationshipSuperheroPower.sql: This script contains statements to create a linking table to set up the relationship between Superheroes and Powers. This script should contain any ALTER statements needed to set up the foreign key constraints between the linking tables and the Superhero and Power tables.
        04. insertSuperheroes.sql: This script inserts three new superheroes into the database.
        05. insertAssistants.sql: This script inserts three assistants and decide on which superheroes they can assist.
        06. powers.sql: This script inserts four powers, then the script needs to give the superheroes powers. One superhero should have multiple powers and one power should be used by multiple superheroes, to demonstrate the many-to-many relationship.
        07. updateSuperhero.sql: This script updates the name of a selected superhero.
        08. deleteAssistant.sql: This script deletes an assistant by name, to ease working with autoincremented numbers.

### Tables

    - Superhero: Contains columns for an autoincremented integer Id, Name, Alias, Origin.
    - Assistant: Contains columns for an autoincremented integer Id, Name.
    - Power: Contains columns for an autoincremented integer Id, Name, Description.

### Table Relationships

  -  One Superhero can have multiple assistants, one assistant has one superhero they assist.
  -  One Superhero can have many powers, and one power can be present on many Superheroes.

### Note

  -  Make sure you have PgAdmin installed and running before you run the scripts.
  -  Also make sure you have SQL installed and running.
  -  You can test the scripts and save them from PgAdmin through the query tool.
  -  Referential integrity will stop us from deleting any records which have got relationships used. Meaning, a superhero cannot be deleted if it is used in the assistant table.
  -  The script 07_updateSuperhero.sql will update the name of the Superhero you choose.
  -  The script 08_deleteAssistant.sql will delete the Assistant you choose by name.

# Chinook

### Introduction

This project is to create a Spring Boot application that interacts with a Chinook database using JDBC and the PostgreSQL driver. The Chinook database models the iTunes database of customers purchasing songs. The application should have a repository pattern to perform various CRUD operations on the database. The media mogul wants to re-make iTunes under a different name, and ensure that the prototype should not cause any legal issues. The application should be externalized in application.properties and accessed via @Value.
Requirements

   - Java
   - Spring Boot
   - PostgreSQL
   - PgAdmin
   - SQL

### Installation

    1. Create a new database in PgAdmin called "Chinook"
    2. Drag and run the Chinook script in PgAdmin's query tool to generate the tables and populate the database.
    3. Clone or download the repository.
    4. Run the command mvn spring-boot:run to start the application

### Usage

The application has a repository pattern to interact with the Chinook database. The repository should provide functionality for customers:

    5. Read all customers in the database, displaying their Id, first name, last name, country, postal code, phone number, and email.
    6. Read a specific customer from the database by Id.
    7. Read a specific customer by name, using the LIKE keyword for partial matches.
    8. Return a page of customers from the database, using limit and offset parameters and the SQL limit and offset keywords.
    9. Add a new customer to the database.
    10. Update an existing customer.
    11. Return the country with the most customers.
    12. Return the customer who is the highest spender, determined by the largest total in the invoice table.
    13. Return the most popular genre for a given customer, determined by the most tracks from invoices associated to that customer.

### Note

    Make sure you have PostgreSQL and PgAdmin installed and running before you run the application.
    Also make sure you have SQL installed and running.
    You can also import the project in any java IDE and run it from there.
    Also make sure you have maven installed and running.
    Make sure you have created the Chinook database before running the application.
    All configuration needs to be externalized in application.properties and accessed via @Value.