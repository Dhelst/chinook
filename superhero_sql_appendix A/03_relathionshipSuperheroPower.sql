DROP TABLE IF EXISTS superhero_power;
CREATE TABLE superhero_power(
hero_id int NOT NULL REFERENCES superhero(hero_id),
power_id int NOT NULL REFERENCES power(power_id),
PRIMARY KEY (hero_id, power_id)	
);